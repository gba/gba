use crate::menu::{create_menu, MenuEntry};
use crate::tests::bios_math;

const ENTRIES: [MenuEntry; 2] = [
	MenuEntry{text: "Test BIOS math functions", action: bios_math::run},
	MenuEntry{text: "Test graphics", action: bios_math::run},
];
pub(crate) fn create_test_menu() {
	create_menu(&ENTRIES);

}
