use gba::bios;
use gba;

use numtoa::NumToA;

struct ArctanTest {
  input: i16,
  output: i16,
}

// TODO: Implement some other math tests
const ATAN_TESTS: [ArctanTest; 2] = [
  ArctanTest { input: 0x0, output: 0x0 },
  ArctanTest { input: 0x1, output: 0x0 }, /*
                                          ArctanTest{ 0x3FFF,     0x1FFF, 0xFFFFC002,  0x8001 },
                                          ArctanTest{ 0x4000,     0x2000, 0xFFFFC000,  0x8000 },
                                          ArctanTest{ 0x4001,     0x1FFF, 0xFFFFBFFE,  0x7FFE },
                                          ArctanTest{ 0x7FFF,     0x16D8, 0xFFFF0004, 0x22DB6 },
                                          ArctanTest{ 0x8000,     0x16A2, 0xFFFF0000, 0x22D45 },
                                          ArctanTest{ 0x8001,     0x1005, 0xFFFEFFFC, 0x22006 },
                                          ArctanTest{ 0xBFFF,     0x1F64, 0x0001C006, 0x17F33 },
                                          ArctanTest{ 0xC000, 0xFFFFC360, 0x0001C000, 0x10480 },
                                          ArctanTest{ 0xC001, 0xFFFFD550, 0x0001BFFA, 0x271BD },
                                          ArctanTest{ 0xFFFF, 0xFFFFA2FE,          8,  0xA2FF },
                                          ArctanTest{ 0xFFFF0000,     0x5D07,          0,  0xA2F9 },
                                          ArctanTest{ 0xFFFF0001,     0x5D01,          8,  0xA2FF },
                                          ArctanTest{ 0xFFFF3FFF,     0x2AAF, 0x0001BFFA, 0x271BD },
                                          ArctanTest{ 0xFFFF4000,     0x3CA0, 0x0001C000, 0x10480 },
                                          ArctanTest{ 0xFFFF4001, 0xFFFFE09B, 0x0001C006, 0x17F33 },
                                          ArctanTest{ 0xFFFF7FFF, 0xFFFFEFFA, 0xFFFEFFFC, 0x22006 },
                                          ArctanTest{ 0xFFFF8000, 0xFFFFE95D, 0xFFFF0000, 0x22D45 },
                                          ArctanTest{ 0xFFFF8001, 0xFFFFE927, 0xFFFF0004, 0x22DB6 },
                                          ArctanTest{ 0xFFFFBFFF, 0xFFFFE000, 0xFFFFBFFE,  0x7FFE },
                                          ArctanTest{ 0xFFFFC000, 0xFFFFE000, 0xFFFFC000,  0x8000 },
                                          ArctanTest{ 0xFFFFC001, 0xFFFFE000, 0xFFFFC002,  0x8001 },
                                          ArctanTest{ 0xFFFFFFFF, 0xFFFFFFFF,          0,  0xA2F9 }
                                          */
];

// TODO: Display test submenu
// TODO: Log result somewhere instead of instantly panicking
pub(crate) fn run() {
  let mut buf: [u8; 32] = [0x0; 32];
  let mut buf2: [u8; 32] = [0x0; 32];
  // Test arctan
  for test in ATAN_TESTS.iter() {
    if bios::atan(test.input) == test.output {
      gba::debug!("Arctan test passed for value {}", test.input.numtoa_str(10, &mut buf));
    } else {
      gba::error!("Arctan test failed! Expected {}, got {}", test.output.numtoa_str(10, &mut buf), bios::atan(test.input).numtoa_str(10, &mut buf2));
    }
  }
}
