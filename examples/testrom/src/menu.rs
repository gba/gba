/// This module implements a very simple, fullscreen menu system.
/// It allows the user to scroll through a list of entries using the D-Pad, and select one using A.
/// Upon selection, the associated function is run.

use core::fmt::Write;

use crate::text::{MAX_Y, ScreenWriter};
use gba::io::keypad;

/// A menu entry is an element that the user may choose and activate.
pub(crate) struct MenuEntry {
	pub(crate) text: &'static str,
	pub(crate) action: fn(), // Implicit function pointer
}

/// Creates a menu with the given entries and displays it, firing the respective function upon user input.
pub(crate) fn create_menu(entries: &[MenuEntry]) {
	// Write entries to screen
	let mut t = ScreenWriter::init();
	
	for e in entries {
		// Note the single space before each entry; It's to make space for the cursor
		write!(t, " {}\n", e.text).unwrap();
	}

	let mut curr_selected_line: usize = 0;
	// Place cursor in top-left corner
	t.put_char_at_pos(0, curr_selected_line, '>');

	loop {
    	let key = wait_for_keypress();
		// Let the user move cursor
		

		// Execute associated function
		match key {
    		Keys::A => {
				(entries[curr_selected_line].action)();
				return;
    		},
    		// TODO: Wrap
    		Keys::Up => {
        		// Remove old cursor
				t.put_char_at_pos(0, curr_selected_line, ' ');


        		if curr_selected_line == 0 {
            		// Back to bottommost line
					curr_selected_line = entries.len() - 1;
        		} else {
					curr_selected_line = curr_selected_line-1;
        		}
    		},
    		Keys::Down => {
        		// Remove old cursor
				t.put_char_at_pos(0, curr_selected_line, ' ');

        		if curr_selected_line == entries.len() - 1 {
					curr_selected_line = 0;
        		} else {
					curr_selected_line = curr_selected_line+1;
        		}
    		},
		}
		t.put_char_at_pos(0, curr_selected_line, '>');
	}
}

enum Keys {
	Up,
	Down,
	A,
}

fn wait_for_keypress() -> Keys {
	loop {
		let keys = keypad::read_key_input();
		if keys.a() {
			return Keys::A;
		}
		if keys.up() {
			return Keys::Up;
		}
		if keys.down() {
			return Keys::Down;
		}	
	}
}
