#![no_std]
#![feature(start)]

mod tests;
mod text;
mod font_data;
mod menu;

extern crate gba;

use gba::fatal;

#[start]
fn main(_argc: isize, _argv: *const *const u8) -> isize {

  // Display a menu
  tests::menu::create_test_menu();

  // If the user didn't choose anything, run all the tests (useful in noninteractive CI).
  tests::bios_math::run();

  // Notify external test harness that we're done
  // TODO: Actually check first
  gba::info!("All tests passed!");

  // Nothing left to do
  loop {}
}

#[panic_handler]
fn panic(info: &core::panic::PanicInfo) -> ! {
  // This kills the emulation with a message if we're running within mGBA.
  fatal!("{}", info);
  // If we're _not_ running within mGBA then we still need to not return, so
  // loop forever doing nothing.
  // TODO: Implement printing of panic to screen/serial port
  loop {}
}
