/*
This module implements a very simple way to print text.
Do not use this for actual games, as it overwrites the entire screen.
Howewer, it's sufficient for a purely text-driven menu system.
*/

use core::fmt::Write;
use crate::font_data::{FONT_PALETE, FONT_TILES, get_index_from_chara};

use gba::{
	io::display::{DisplayMode, DisplayControlSetting, DISPCNT},
	io::background::{BackgroundControlSetting, BG0CNT},
	vram,
	vram::text::{TextScreenblockEntry},
	palram,
};
use voladdress::VolBlock;
use typenum::consts::{U1024};


use core::convert::TryInto;

// Tiles are 8x8
pub(crate) const MAX_X: usize = 256 / 8;
pub(crate) const MAX_Y: usize = 160 / 8;

const NUM_TILES: usize = 96;

// TODO: Color, modified text
pub(crate) struct ScreenWriter {
	curr_x: usize,
	curr_y: usize,
	tilemap: VolBlock<vram::text::TextScreenblockEntry, U1024>,
}

impl ScreenWriter {
    /// Turns the screen into a new text buffer.
    /// This action overwrites all screen contents!
	pub(crate) fn init() -> ScreenWriter {
		// Switch to mode 0
		const MODE: DisplayControlSetting =
			DisplayControlSetting::new().with_mode(DisplayMode::Mode0).with_bg0(true);
		DISPCNT.write(MODE);

		// Move font into charblock memory
		// In a real game this would be a function argument, to avoid clobbering user tiles
		let charblock = vram::get_4bpp_character_block(0);
		// FIXME: Do this in a more structured way
		for (i, b) in charblock.iter().enumerate() {
    		// Load entire tileset into charblock, pad the rest out with blank tiles
			let mut tile: [u32; 8] = [0x0;8];
			if i < NUM_TILES { 
				for j in 0..8 {
					tile[j] = FONT_TILES[(i*8)+j];
     			}
			}
			b.write(vram::Tile4bpp(tile));
		}

		// Move palete into PALRAM
		for (i, entry) in FONT_PALETE.iter().enumerate() {
			let idx = palram::index_palram_bg_4bpp(0, i.try_into().unwrap());
			idx.write(gba::Color(*entry));
		}

		// Allocate and clear the tilemap to be used for text display
		let tilemap = vram::get_screen_block(31); // Important to choose one that's not used by charblock!
		for entry in tilemap.iter() {
			entry.write(TextScreenblockEntry::new());
		}

		// Set BG0 control register to use proper options
		const BG_SETTINGS: BackgroundControlSetting = BackgroundControlSetting::new()
		.with_bg_priority(0)
		.with_char_base_block(0)
		.with_screen_base_block(31);
		BG0CNT.write(BG_SETTINGS);

		return ScreenWriter { curr_x: 0, curr_y: 0, tilemap: tilemap };
	}

	/// This function writes text, line-by-line.
	fn write(&mut self, s: &str) {
		for c in s.chars() {
			self.write_char(c);
		}
	}

	fn write_char(&mut self, c: char) {
		// Check whether to advance to next line because line is full
		if self.curr_x > MAX_X {
			self.curr_x = 0;
			self.curr_y = self.curr_y + 1;
		}

		// Check whether to advance to next line because of a newline
		match c {
			'\n' => {
				self.curr_x = 0;
				// Scroll down if the next line wouldn't fit
				if (self.curr_y + 1) >= MAX_Y {
					self.scroll_down();
				} else {
					self.curr_y = self.curr_y + 1;
				}
			}
			// Otherwise, just write character at current position
			_ => {
				self.write_at_curr_pos(c);
				self.curr_x = self.curr_x + 1;
			}
		}
	}
		/// Scroll down by 1 line.
		/// TODO: Consider making this public
		fn scroll_down(&mut self) {
			// Use hardware scrolling to treat the tilemap as a ring buffer of sorts (wraparound is done by the HW)

			// If we are at the end of
			unimplemented!();
		}
	fn write_at_curr_pos(&mut self, c: char) {
    	self.put_char_at_pos(self.curr_x, self.curr_y, c);
    }

	/// Write a character at position (x, y), overwriting anything that was there before.
	/// x and y have to be <= MAX_X and MAX_Y, or you will get a panic.
    pub(crate) fn put_char_at_pos(&mut self, x: usize, y: usize, c: char) {
		if (x > MAX_X) || (y > MAX_Y) {
			panic!("Attempt to write character out of bounds!");
		}

    	// Calculate the index of the tile we want to write in the tilemap
    	let tilemap_index: usize = (y*MAX_X)+x;
    	
    	// Look up where in the tile block this character is
    	let tileblock_index = TextScreenblockEntry::from_tile_id(get_index_from_chara(c).try_into().unwrap());
		self.tilemap.index(tilemap_index).write(tileblock_index);
		
    }
}

impl Write for ScreenWriter {
	fn write_str(&mut self, s: &str) -> core::fmt::Result {
		self.write(s);
		return Ok(());
	}
}
